<!doctype html>
<html class="no-js">
  <head>
    <meta charset="utf-8">
    <title>Developed by Tom Harms Development</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0, minimal-ui">

    <link href="src/css/style.css" rel="stylesheet">

    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>
    <script src="//ajax.googleapis.com/ajax/libs/webfont/1.5.6/webfont.js"></script>
    <script>
      WebFont.load({
        custom: {
          families: ['signikaregular'],
          urls: ['src/fonts/Signika/signika.css']
      }
    });
  </script>

  </head>
  <body>
  <div class="se-pre-con"></div>
<header class="header" role="banner">
  <p>Run to Date</p>
  <a class="strava-btn" href="https://www.strava.com/oauth/authorize?client_id=5545&response_type=code&redirect_uri=http://student.cmi.hro.nl/0853538/RuntoDate/&scope=write&state=mystate&approval_prompt=auto" target="_self"><img src="src/img/strava.png"></a>
  <img id="pf--img" src="">
</header>
