module.exports = function(grunt) {
  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
 
 
    /*------------------------------------*\
        #minifying SASS and Javascript
    \*------------------------------------*/
 
    sass: {
        options: {
            sourceMap: true,
            style: 'compressed'
        },
        dist: {
            files: {
                'src/css/style.css': 'src/scss/style.scss'
            }
        }
    },
 
    uglify: {
      my_target: {
        files: {
          'src/js/min/scripts-min.js': 'src/js/scripts.js'
        }
      }
    },
 
 
    /*-------------------------------------------*\
        #Watch scss and javascript for changes
    \*-------------------------------------------*/
     
 
    watch: {
      scss: {
        files: 'src/scss/**/*.scss',
        tasks: ['notify:compiling', 'sass', 'notify:watch'],
      },
      javacript: {
        files: 'src/js/*.js',
        tasks: ['notify:compiling', 'uglify', 'notify:watch'],
      }
    },
 
 
    /*-------------------------------------------------*\
        #Starting multiple functions at the same time
    \*-------------------------------------------------*/
 
 
    concurrent: {
      dev: {
        tasks: ['watch','browserSync:NoScroll', 'notify:dev'],
        options: {
          logConcurrentOutput: true
        }
      },
      testing: {
        tasks: ['browserSync', 'watch', 'notify:testing'],
        options: {
          logConcurrentOutput: true
        }
      }
    },
 
 
    /*----------------------------------------------------------------*\
        #File injection en syncen multiple browsers at the same time
    \*----------------------------------------------------------------*/
 
 
    browserSync: {
      bsFiles: {
        src : ["src/css/*.css","*.php", "src/js/min/*.js"],
      },
 
      NoScroll: {
        bsFiles: {
          src : ["src/css/*.css","app/**/*.php",'*.php', "src/js/min/*.js"],
        },
        options: {
          options: {
            proxy: "192.168.0.7"
          },
          ghostMode: {
            clicks: true,
            location: true,
            forms: true,
            scroll: false
          }
        }
      },
    },
 
 
    /*--------------------------------------------------------------------------*\
        #Notify for grunt, notifies when task start and when the are finished
    \*--------------------------------------------------------------------------*/
 
 
    notify: {
     watch: {
      options: {
        title: 'Task Complete',  // optional
        message: 'SASS compiled', //required
      }
    },
    compiling: {
      options: {
        title: 'Task started',  // optional
        message: 'Compiling', //required
      }
    },
    testing: {
      options: {
        title: 'Testing started',  // optional
        message: 'Start testing', //required
      }
    },
    dev: {
      options: {
        title: 'Development started',  // optional
        message: 'Start developing', //required
      }
    },
    build: {
      options: {
        title: 'Compress finished',
        message: 'uglify & sass compressed',
      }
    }
  }
 
});
 
 
  /*----------------------------------------------------------------*\
      #All grunt tasks are loaded down here.
  \*----------------------------------------------------------------*/
 
 
  grunt.loadNpmTasks('grunt-browser-sync');
  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-concurrent');
  grunt.loadNpmTasks('grunt-notify');
 
 
  /*----------------------------------------------------------------*\
      #Tasks that users can run
  \*----------------------------------------------------------------*/
 
   
  //Use this task when developing
  grunt.registerTask('dev', ['concurrent:dev']);
 
  //Use this task when testing
  grunt.registerTask('testing', ['concurrent:testing']);
 
  //Use this task when the project is finished
  grunt.registerTask('build', ['notify:build','sass', 'uglify']);
};