"use strict";function HandleData(a){var b=JSON.parse(a),c=b.access_token;GetUserData(c)}function GetUserData(a){$.ajax({url:"https://www.strava.com/api/v3/athlete/?&access_token="+a+"&callback=?",type:"GET",dataType:"jsonp",error:function(){alert("error")},success:function(a){$.ajax({type:"POST",url:"/user/Adduserdata.php",data:{userfname:a.firstname,userlname:a.lastname,usercity:a.city,usersex:a.sex,userimg:a.profile,useremail:a.email},error:function(){alert("error")},success:function(){pfimg=a.profile,pfname=a.firstname+a.lastname,pfcity=a.city,pfsex=a.sex,window.usr=a.email,ParseData(pfimg,pfname,pfcity,pfsex)}})}})}function ParseData(a,b,c,d){$("#pf--img").attr("src",a),$("#pf--name").text(b),$("#pf--city").text(c),$("#pf--sex").text(d)}function createMap(a,b,c){function d(a){console.log(a),clearMarkers();var b=new google.maps.Marker({position:a,map:map});markers.push(b),window.lat=a.k,window.lng=a.D}function e(a,b){{var c=new google.maps.LatLng(b,a);new google.maps.Marker({position:c,map:map})}}function f(){$.ajax({type:"GET",url:"/user/Geteventdata.php",error:function(){console.log("error")},success:function(a){var b=JSON.parse(a);$.each(b.Dates,function(){eventlat=this.lat,eventlng=this.lng,e(eventlng,eventlat)})}})}f();var g={center:new google.maps.LatLng(a,b),zoom:c,scrollwheel:!1,mapTypeId:google.maps.MapTypeId.ROADMAP};map=new google.maps.Map(document.getElementById("map-canvas"),g);var h=document.getElementById("pac-input");map.controls[google.maps.ControlPosition.TOP_LEFT].push(h);var i=new google.maps.places.SearchBox(h);google.maps.event.addListener(i,"places_changed",function(){var a=i.getPlaces();if(clearMarkers(),0!=a.length){for(var b,d=new google.maps.LatLngBounds,e=0;b=a[e];e++){var f={url:b.icon,size:new google.maps.Size(71,71),origin:new google.maps.Point(0,0),anchor:new google.maps.Point(17,34),scaledSize:new google.maps.Size(25,25)},g=new google.maps.Marker({map:map,icon:f,title:b.name,position:b.geometry.location});markers.push(g),d.extend(b.geometry.location)}map.fitBounds(d),map.setZoom(c)}}),google.maps.event.addListener(map,"click",function(a){d(a.latLng)})}function initialize(){navigator.geolocation&&(success=function(a){createMap(a.coords.latitude,a.coords.longitude,15);{var b=new google.maps.LatLng(a.coords.latitude,a.coords.longitude);new google.maps.InfoWindow({map:map,position:b,content:"Location found using HTML5."})}},error=function(){createMap(99.648493,-99.410812,12)},navigator.geolocation.getCurrentPosition(success,error))}function setAllMap(a){for(var b=0;b<markers.length;b++)markers[b].setMap(a),console.log(markers.length)}function clearMarkers(){setAllMap(null)}function validateInput(a){var b=a.target.getAttribute("min-length"),c=a.target.getAttribute("max-length"),d="text-indent: 0; border-bottom: 3px solid rgba(0, 163, 136, 0.95); color: rgba(0, 163, 136, 0.95);",e="text-indent: 0; border-bottom: 3px solid rgba(242, 48, 65, 0.95); color: rgba(242, 48, 65, 0.95);";if(input.hide($("."+a.target.parentNode.className+" .char-counter")),"input"===a.target.tagName.toLowerCase()?$("."+a.target.parentNode.className+" label").style.marginTop="-90px":"textarea"===a.target.tagName.toLowerCase()&&($("."+a.target.parentNode.className+" label").style.marginTop="-105px"),"input"===a.target.tagName.toLowerCase()&&"submit"!==a.target.type){if("text"===a.target.type){if(pattern.name.test(a.target.value)===!0&&""!==a.target.value)return input.say("."+a.target.parentNode.className+" .notification","",0),a.target.style.cssText=d,!0;if(pattern.name.test(a.target.value)===!1&&""!==a.target.value)return input.say("."+a.target.parentNode.className+" .notification",errorMsg.invalid,1),a.target.style.cssText=e,!1}else if("email"===a.target.type){if(pattern.email.test(a.target.value)===!0&&""!==a.target.value)return input.say("."+a.target.parentNode.className+" .notification","",0),a.target.style.cssText=d,!0;if(pattern.email.test(a.target.value)===!1&&""!==a.target.value)return input.say("."+a.target.parentNode.className+" .notification",errorMsg.email,1),a.target.style.cssText=e,!1}else if("url"===a.target.type&&""!==a.target.value){if(pattern.url.test(a.target.value)&&""!==a.target.value)return input.say("."+a.target.parentNode.className+" .notification","",0),a.target.style.cssText=d,!0;if(!pattern.url.test(a.target.value))return input.say("."+a.target.parentNode.className+" .notification",errorMsg.url,1),a.target.style.cssText=e,!1}}else if("textarea"===a.target.tagName.toLowerCase()){if(""!==a.target.value&&a.target.value.length>=b&&a.target.value.length<=c)return input.say("."+a.target.parentNode.className+" .notification","",0),a.target.style.cssText=d,!0;if(""!==a.target.value&&a.target.value.length<b)return input.say("."+a.target.parentNode.className+" .notification",errorMsg.tooShort,1),a.target.style.cssText=e,!1;if(""!==a.target.value&&a.target.value.length>c)return input.say("."+a.target.parentNode.className+" .notification",errorMsg.tooLong,1),a.target.style.cssText=e,!1}return""===a.target.value&&"submit"!==a.target.type?"url"===a.target.type?(input.hide($("."+a.target.parentNode.className+" .notification")),a.target.style.cssText=d,a.target.placeholder="n/a",!0):(input.say("."+a.target.parentNode.className+" .notification",errorMsg.required,1),a.target.style.cssText=e,!1):void 0}function readyInput(a){("input"===a.target.tagName.toLowerCase()||"textarea"===a.target.tagName.toLowerCase())&&(input.hide($("."+a.target.parentNode.className+" .notification")),input.show($("."+a.target.parentNode.className+" .char-counter")),a.target.style.cssText="text-indent: 0; border-bottom: 3px solid  rgba(0, 161, 217, 1); color: #777;")}function formCharCounter(a){"input"!==a.target.tagName.toLowerCase()&&"textarea"!==a.target.tagName.toLowerCase()||"submit"===a.target.type||textCounter($("."+a.target.className),$("."+a.target.parentNode.className+" .char-counter"))}function textCounter(a,b){var c=a.getAttribute("max-length");a.value.length>c?a.value=a.value.substring(0,c):b.textContent=c-a.value.length+"/"+c+" chars"}function $(a){return document.querySelector(a)}var lng,lat,usr,markers=[];$(document).ready(function(){initialize();var a=document.URL,b=a.split("=")[2];"undefined"==b||"access_denied"==b?console.log(error):$.ajax({type:"POST",url:"http://localhost/user/userinfo.php",data:{result:b},error:function(){alert("error")},success:function(a){HandleData(a)}})}),$("form").submit(function(a){Evenementnaam=$("#Field1").val(),Beschrijving=$("#Field2").val(),Time=$("#field3").val(),Datestamp=$("#field4").val(),PublicBool=0,$("input[id=Field3_0]:checked").length>0&&(PublicBool=1),console.log(Evenementnaam),console.log(Beschrijving),console.log("datum: "+Datestamp),console.log("tijd: "+Time),console.log(PublicBool),console.log(window.lat),console.log(window.lng),console.log(window.usr),$.ajax({type:"POST",url:"/user/Addeventdata.php",data:{evenementnaam:Evenementnaam,beschrijving:Beschrijving,datestamp:Datestamp,time:Time,publicbool:PublicBool,lng:window.lng,lat:window.lat,usr:window.usr},error:function(){console.log("error")},success:function(a){console.log("event toegevoegd"),console.log(a)}}),a.preventDefault()});var form=$("#contact-form");form.addEventListener("blur",validateInput,!0),form.addEventListener("focus",readyInput,!0),form.addEventListener("focus",formCharCounter,!0),form.addEventListener("keyup",formCharCounter,!0),form.addEventListener("keydown",formCharCounter,!0);var errorMsg={invalid:"Invalid input.",required:"This field is required.",url:"Invalid url.",email:"E-mail is invalid.",tooShort:"Input is too short.",tooLong:"Input is too long."},pattern={name:/[A-Za-z -']$/,email:/^(([-\w\d]+)(\.[-\w\d]+)*@([-\w\d]+)(\.[-\w\d]+)*(\.([a-zA-Z]{2,5}|[\d]{1,3})){1,2})$/,url:/^(https?:\/\/)?([\w\d\-_]+\.+[A-Za-z]{2,})+\/?/},input={say:function(a,b,c){var d=$(a);d.textContent=b,d.style.opacity=c},show:function(a){return a.style.opacity="1"},hide:function(a){return a.style.opacity="0"}};;'use strict';

var form = $("#contact-form");


//event listeners
form.addEventListener('blur', validateInput, true);
form.addEventListener('focus', readyInput, true);
form.addEventListener('focus', formCharCounter, true);
form.addEventListener('keyup', formCharCounter, true);
form.addEventListener('keydown', formCharCounter, true);

//error messages
var errorMsg = {
    invalid: 'Invalid input.',
    required: 'This field is required.',
    url: 'Invalid url.',
    email: 'E-mail is invalid.',
    tooShort: 'Input is too short.',
    tooLong: 'Input is too long.'
};

//regex stuff
var pattern = {
    name: /[A-Za-z -']$/,
    email: /^(([-\w\d]+)(\.[-\w\d]+)*@([-\w\d]+)(\.[-\w\d]+)*(\.([a-zA-Z]{2,5}|[\d]{1,3})){1,2})$/,
    url: /^(https?:\/\/)?([\w\d\-_]+\.+[A-Za-z]{2,})+\/?/
};

//a few utitlies
var input = {
    say: function (el, msg, visible) {
        var field = $(el);

        field.textContent = msg;
        field.style.opacity = visible;
    },
    show: function (el) {
        return el.style.opacity = '1';
    },
    hide: function (el) {
        return el.style.opacity = '0';
    }
};

//validation 
function validateInput(e) {
    var commentMin = e.target.getAttribute('min-length');
    var commentMax = e.target.getAttribute('max-length');
    var applyValid = 'text-indent: 0; border-bottom: 3px solid rgba(0, 163, 136, 0.95); color: rgba(0, 163, 136, 0.95);';
    var applyError = 'text-indent: 0; border-bottom: 3px solid rgba(242, 48, 65, 0.95); color: rgba(242, 48, 65, 0.95);';

    //hide char counter
    input.hide($('.' + e.target.parentNode.className + ' .char-counter'));

    //Align labels
    if (e.target.tagName.toLowerCase() === 'input') {
        ($('.' + e.target.parentNode.className + ' label')).style.marginTop = '-90px';
    } else if (e.target.tagName.toLowerCase() === 'textarea') {
        ($('.' + e.target.parentNode.className + ' label')).style.marginTop = '-105px';
    }

    if (e.target.tagName.toLowerCase() === 'input' && e.target.type !== 'submit') {
        //test input fields, probably better targeting input classes/id
        //if the form happens to have multiple fields of the same type
        if ((e.target.type) === 'text') {
            if (pattern.name.test(e.target.value) === true && e.target.value !== '') {
                input.say('.' + e.target.parentNode.className + ' .notification', '', 0);
                e.target.style.cssText = applyValid;
                return true;
            } else if (pattern.name.test(e.target.value) === false && e.target.value !== '') {
                input.say('.' + e.target.parentNode.className + ' .notification', errorMsg.invalid, 1);
                e.target.style.cssText = applyError;
                return false;
            }
        } else if ((e.target.type) === 'email') {
            if (pattern.email.test(e.target.value) === true && e.target.value !== '') {
                input.say('.' + e.target.parentNode.className + ' .notification', '', 0);
                e.target.style.cssText = applyValid;
                return true;
            } else if (pattern.email.test(e.target.value) === false && e.target.value !== '') {
                input.say('.' + e.target.parentNode.className + ' .notification', errorMsg.email, 1);
                e.target.style.cssText = applyError;
                return false;
            }
        } else if ((e.target.type) === 'url' && e.target.value !== '') {
            if (pattern.url.test(e.target.value) && e.target.value !== '') {
                input.say('.' + e.target.parentNode.className + ' .notification', '', 0);
                e.target.style.cssText = applyValid;
                return true;
            } else if (!pattern.url.test(e.target.value)) {
                input.say('.' + e.target.parentNode.className + ' .notification', errorMsg.url, 1);
                e.target.style.cssText = applyError;
                return false;
            }
        }
    } else if (e.target.tagName.toLowerCase() === 'textarea') {
        if (e.target.value !== '' && (e.target.value.length >= commentMin && e.target.value.length <= commentMax)) {
            input.say('.' + e.target.parentNode.className + ' .notification', '', 0);
            e.target.style.cssText = applyValid;
            return true;
        } else if (e.target.value !== '' && e.target.value.length < commentMin) {
            input.say('.' + e.target.parentNode.className + ' .notification', errorMsg.tooShort, 1);
            e.target.style.cssText = applyError;
            return false;
        } else if (e.target.value !== '' && e.target.value.length > commentMax) {
            input.say('.' + e.target.parentNode.className + ' .notification', errorMsg.tooLong, 1);
            e.target.style.cssText = applyError;
            return false;
        }
    }

    //if fields are empty except url which is optional
    if (e.target.value === '' && e.target.type !== 'submit') {
        if (e.target.type === 'url') {
            input.hide($('.' + e.target.parentNode.className + ' .notification'));
            e.target.style.cssText = applyValid;
            e.target.placeholder = 'n/a';
            return true;
        } else {
            input.say('.' + e.target.parentNode.className + ' .notification', errorMsg.required, 1);
            e.target.style.cssText = applyError;
            return false;
        }
    }
}

// hide notification and reveal char count
function readyInput(e) {
    if ((e.target.tagName.toLowerCase() === 'input') || (e.target.tagName.toLowerCase() === 'textarea')) {
        input.hide($('.' + e.target.parentNode.className + ' .notification'));
        input.show($('.' + e.target.parentNode.className + ' .char-counter'));
        e.target.style.cssText = 'text-indent: 0; border-bottom: 3px solid  rgba(0, 161, 217, 1); color: #777;'
    }
}

//apply char count
function formCharCounter(e) {
    if ((e.target.tagName.toLowerCase() === 'input' || e.target.tagName.toLowerCase() === 'textarea') && e.target.type !== 'submit') {
        textCounter($('.' + e.target.className), $('.' + e.target.parentNode.className + ' .char-counter'));
    }
}

//char count
function textCounter(field, counter) {
    var maxLength = field.getAttribute('max-length');

    if (field.value.length > maxLength) {
        field.value = field.value.substring(0, maxLength);
    } else {
        counter.textContent = maxLength - field.value.length + "/" + maxLength + ' chars';
    }
}

function $(q) {
    return document.querySelector(q);
}