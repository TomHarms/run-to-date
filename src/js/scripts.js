var lng;
var lat;
var usr;
var markers = [];


$( document ).ready(function() {
	initialize();
	var url = document.URL;
	var result = url.split("=")[2];

	if (result == "undefined" || result == "access_denied") {
		console.log(error);
	}
	else {
		$.ajax({
			type: 'POST',
			url: 'user/userinfo.php',
			data: {result:result},
			error: function(xhr, status, error) {
				console.log("error");
			},
			success: function(json, test) {
				HandleData(json);
			}
		});
	}
});


	$(window).load(function() {
		// Animate loader off screen
		 setTimeout(function(){
		 	$(".se-pre-con").fadeOut("slow");},2500);		
	});


function HandleData(data){
	var KeyData = JSON.parse(data);

	var accesstoken = KeyData.access_token;
	GetUserData(accesstoken);
}


function GetUserData(accesskey) {
	$.ajax({
		url: 'https://www.strava.com/api/v3/athlete/?&access_token=' + accesskey + '&callback=?',
		type: 'GET',
		dataType: 'jsonp',
		error: function(xhr, status, error) {
			console.log("error");
		},
		success: function(json) {

			$.ajax({
				type: 'POST', 
				url: 'user/Adduserdata.php',
				data: {
					userfname : json.firstname,
					userlname : json.lastname,
					usercity  : json.city,
					usersex   : json.sex,
					userimg   : json.profile,
					useremail : json.email 
				},
				error: function(xhr, status, error) {
					console.log("error");
				},
				success: function() {	
					pfimg = json.profile;
					pfname = json.firstname + json.lastname;
					pfcity = json.city;
					pfsex = json.sex;
					
					window.usr = json.email;

					ParseData( pfimg, pfname, pfcity, pfsex);
				}
			});
		}
	});
}

function ParseData ( pfimg, pfname, pfcity, pfsex ) {
	$("#pf--img").attr("src", pfimg);
	$('.strava-btn').addClass('nodisplay');
	$('#pf--img').addClass('displayblock');
}

function createMap(lat, lng, zoomVal) {

	getEvents();

	var mapOptions = { 
		center: new google.maps.LatLng(lat, lng),    
		zoom: zoomVal,   
		scrollwheel: false,  
		mapTypeId: google.maps.MapTypeId.ROADMAP 
	}; 

	map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions); 

	// Create the search box and link it to the UI element.
	var input = /** @type {HTMLInputElement} */(
		document.getElementById('pac-input'));
	map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

	var searchBox = new google.maps.places.SearchBox(
		/** @type {HTMLInputElement} */(input)); 

	google.maps.event.addListener(searchBox, 'places_changed', function() {
		var places = searchBox.getPlaces();
		clearMarkers();
		if (places.length === 0) {
			return;
		}

    // For each place, get the icon, place name, and location.
    var bounds = new google.maps.LatLngBounds();
    for (var i = 0, place; place = places[i]; i++) {
    	var image = {
    		url: place.icon,
    		size: new google.maps.Size(71, 71),
    		origin: new google.maps.Point(0, 0),
    		anchor: new google.maps.Point(17, 34),
    		scaledSize: new google.maps.Size(25, 25)
    	};

      // Create a marker for each place.
      var marker = new google.maps.Marker({
      	map: map,
      	icon: 'src/img/userlocation.png',
      	title: place.name,
      	position: place.geometry.location
      });

      markers.push(marker);
      bounds.extend(place.geometry.location);
  }
  map.fitBounds(bounds);
  map.setZoom (zoomVal);
});

	google.maps.event.addListener(map, 'click', function(event) {
		placeMarker(event.latLng);
	});

	function placeMarker(location) {
		clearMarkers();
		var marker = new google.maps.Marker({
			position : location, 
			map : map,
			icon : 'src/img/userlocation.png'
		});
		markers.push(marker);

		window.lat = location.k;
		window.lng = location.D;
	}

	function placeEventMarker(lng, lat, eventname, eventdesc, eventdate, eventtime) {
		var eventlatlng = new google.maps.LatLng(lat, lng);

		var marker = new google.maps.Marker({
			position : eventlatlng, 
			map : map,
			icon: 'src/img/events.png'
		});

		var contentString = 
			"Eventnaam: " + eventname + "<br>" +
			"Beschrijving: " + eventdesc + "<br>" +
			"Datum: " + eventdate + "<br>" +
			"Tijd: " + eventtime + "<br>" +
			""
			;

		var infowindow = new google.maps.InfoWindow({
			content: contentString
		});

		google.maps.event.addListener(marker, 'click', function() {
			infowindow.open(map,marker);
		});
	}

	function getEvents() {
		$.ajax({
			type: 'GET',
			url: 'user/Geteventdata.php',

			error: function(xhr, status, error) {
				console.log("error");
			},
			success: function(data) {	
				var jsondata = JSON.parse(data);

				$.each(jsondata.Dates, function() {
					eventlat = this.lat;
					eventlng = this.lng;
					eventname = this.name;
					eventdesc = this.description;
					eventtime = this.time;
					eventdate = this.datestamp.split(/[-]/);

					eventdate = eventdate[2] + "-" + eventdate[1] + "-" + eventdate[0];

					placeEventMarker(eventlng, eventlat, eventname, eventdesc, eventdate, eventtime);
				});
			}
		});
	}

	$( "form" ).submit(function( event ) {

	Evenementnaam 	= 	$( "#Field1" ).val();
	Beschrijving 	= 	$( "#Field2" ).val();
	Time			= 	$( "#Field3" ).val();
	Datestamp 		= 	$( "#Field4" ).val();
	PublicBool 		=	0;

	if ($('input[id=Field3_0]:checked').length > 0) {
		PublicBool = 1;
	}


	$.ajax({
		type: 'POST',
		url: 'user/Addeventdata.php',
		data: {
			evenementnaam 	:  	Evenementnaam,
			beschrijving  	:  	Beschrijving,
			datestamp 		:   Datestamp,
			time 			:   Time,
			publicbool    	:  	PublicBool,
			lng   		  	: 	window.lng,
			lat   		  	: 	window.lat,
			usr 			:   window.usr
		},
		error: function(xhr, status, error) {
			console.log("error");
		},
		success: function(data) {	
			$( "#Field1" ).val('');
			$( "#Field2" ).val('');
			$( "#Field3" ).val('');				
			$( "#Field4" ).val('');
			$("#Field3_0").prop("checked", false);
			$("#Field3_1").prop("checked", true);
			getEvents();
			placeMarker(new google.maps.LatLng("0", "0"));
			alert("event toegevoegd");
		}
	});

	event.preventDefault();
});

}

function initialize() { 
	if(navigator.geolocation) {  
		success = function(position) {    
			createMap(position.coords.latitude, position.coords.longitude, 15);
			
			var pos = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
			var marker = new google.maps.Marker({
				position : pos, 
				map : map,
				icon : 'src/img/location.png'
			});
		};   
		error = function() {
			createMap(99.648493, -99.410812,12); 
		};  
		navigator.geolocation.getCurrentPosition(success, error);  
	}
}



// Sets the map on all markers in the array.
function setAllMap(map) {
	for (var i = 0; i < markers.length; i++) {
		markers[i].setMap(map);
	}
}

// Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
	setAllMap(null);
}
