<?php include('app/sections/header.php'); ?>
<div class="wrapper">

<!-- 	<div class="userinfo">
		<h1>Profiel</h1>
		<img id="pf--img" src="">
		<span id="pf--name"></span>
		<span id="pf--city"></span>
		<span id="pf--sex"></span>
	</div> -->
  <p class="intro">Welkom bij Run to Date, de plek om nieuwe hardloop vrienden te maken.
Voeg hieronder de gegevens in van de startplek of kies een startplek en meld je zelf aan!</p>
	<input id="pac-input" class="controls" type="text" placeholder="Search Box">
	<div id="map-canvas"></div>

	<div class="container">
		<form id="contact-form" class="contact-form" action="#" method="post">

			 <ul>

			    <li class="name">
                    <input type="text" 
                           id="Field1"
                           class="input-name"
                           name="name"
                           placeholder="vb. Rondje lopen"  
                           spellcheck="false" 
                           min-length="2" 
                           max-length="40"
                           required
                    />
                    <label id="name" for="name">Evenement naam</label>
                    <span class="notification"></span>
                    <span class="required"><strong><small><em> (verplicht)</em></small></strong></span>
                    <span class="char-counter"></span>
                </li>

                <li class="comment">
                    <!--Tip: textarea breaks if tags are not in the same line or broken up somehow -->
                    <textarea 	class="text-comment" rows="2" id="Field2" spellcheck="false" min-length="2" max-length="500" required></textarea>
                    <label for="message">Beschrijving</label>
                    <span class="notification"></span>
                    <span class="required"><strong><small><em> (verplicht)</em></small></strong></span>
                    <span class="char-counter"></span>
                </li>

                <li class="time">
                    <input type="time" 
                           id="Field3"
                           class="input-time"
                           name="eventtime"
                           required
                    />
                    <label id="name" for="name">Tijd</label>
                    <span class="notification"></span>
                    <span class="required"><strong><small><em> (verplicht)</em></small></strong></span>
                </li>

                <li class="date">
                    <input type="date"
                           id="Field4" 
                           class="input-date"
                           name="eventdate"
                           required
                    />
                    <label id="name" for="name">Datum</label>
                    <span class="notification"></span>
                    <span class="required"><strong><small><em> (verplicht)</em></small></strong></span>
                </li>

                <li class="public">
                    <label id="name" for="name">Openbaar evenement?</label>
                    <span class="notification"></span>
                    <span class="required"><strong><small><em> (verplicht)</em></small></strong></span>
						<div class="choice">
							<input id="Field3_0" name="Field3" type="radio" value="1">
							<span class="choice" for="Field3_0">Ja</span>
						</div>

						<div class="choice">
							<input id="Field3_1" name="Field3" type="radio" value="0" checked="checked">
							<span class="choice" for="Field3_1">Nee</span>
						</div>

                </li>
            </ul>

             <div class="form-btn">
                <input id="saveForm" name="saveForm" class="input-submit" type="submit" value="Verzend" id="submit" />
            </div>

		</form>
	</div>

</div>
<?php include('app/sections/footer.php'); ?>
